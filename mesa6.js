function mesa1(nomDiv){
  this.divMesaJugador = nomDiv;
  this.cantCartas = 0;
  this.sumaJugador = 0;
}
function mesaBot(nomDiv){
  this.divMesaBot = nomDiv;
  this.cantCartas = 0;
  this.sumaBot= 0;
}

mesa1.prototype.insertarCartaJugador = function(objCarta, lblPunteoJugador){
  var areaCartasJugador = document.getElementById(this.divMesaJugador);
  areaCartasJugador.appendChild(objCarta.dibujar());
  this.sumaJugador = this.sumaJugador + objCarta.darNumero();
  lblPunteoJugador.innerHTML = "PUNTEO JUGADOR: "+this.sumaJugador;
  if(this.sumaJugador>=22){
    lblPunteoJugador.innerHTML = "Has perdido";
    this.sumaJugador=0;
     document.getElementById('btnDarCarta').disabled = true;
  }
  if(this.sumaJugador==21){
    lblPunteoJugador.innerHTML="Jugador GANA";
  }
  console.log("suma:"+this.sumaJugador);
  
}


mesaBot.prototype.insertarCartaBot = function (objCarta,lblPunteoBot) {
  var areaCartasBot = document.getElementById(this.divMesaBot);
  areaCartasBot.appendChild(objCarta.dibujar());
  this.sumaBot = this.sumaBot + objCarta.darNumero();
  lblPunteoBot.innerHTML = "PUNTEO CONTRINCANTE: "+this.sumaBot;
  if(this.sumaBot>=22){
    lblPunteoBot.innerHTML = "Bot pierde!";
    this.sumaBot = 0;
  }
  if(this.sumaBot==21){
    lblPunteoBot.innerHTML="Bot GANA";
  }
  console.log("SumaBot:"+this.sumaBot);

}

