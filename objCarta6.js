function classCarta(){
  this.numero=null;
  this.simbolo = null;  
  this.clase="";
}

classCarta.prototype.definir=function(num,sim){
  this.numero = num;
  this.simbolo = sim;
  this.clase="carta"+this.numero;
}

classCarta.prototype.darNumero=function(){
	return this.numero;	
}

classCarta.prototype.dibujar=function(){	
	var divCarta = document.createElement('div');
	var posy=0;
	var posx=((this.numero-1)*-72);
	if(this.simbolo=="trebol"){
		posy=0;
	}
	if(this.simbolo=="espada"){
		posy=-96;
	}
	if(this.simbolo=="corazon"){
		posy=-192;
	}
	if(this.simbolo=="diamante"){
		posy=-288;
	}	
	divCarta.className="carta";
	divCarta.style.backgroundPosition=posx+"px "+posy+"px";
	return divCarta;
}